const data = [ /*Because reading .JSON is a struggle*/
    {
        "name" : "Mozilla Firebox",
        "price":"451",
        "features": ["Next level Firewall", "Great for burning DvDs", "No cooling required."],
        "description": "No cooling required",
        "image": "pc_fire.jpg"
    },
    {
        "name" : "Cardboard PCa",
        "price":"120",
        "features": ["Smells like Pizza", "Chassy of 100% recyled material", "Highly Customizable"],
        "description": "Cardboard is both ecological and economical!",
        "image": "pc_cardboard.jpg"
    },
    {
        "name" : "Nininedo Wii",
        "price":"500",
        "features": ["Controller interface", "Runs Mario Kart", "Wriststrap included"],
        "description": "The perfect bootleg emulator!",
        "image": "pc_wii.jpg"
    },
    {
        "name" : "Megachonk",
        "price":"999",
        "features": ["Weights less than most cars", "Can be flipped for makeshift shelter", "Earthquake resistant up to 7.3 Reichter scale"],
        "description": "The perfect perfect laptop for users with big laps!",
        "image": "pc_huge.jpg"
    }
];