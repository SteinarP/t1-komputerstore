//#region BANKING
let balance = 200;
let debt = 0;
let loansBeforeBuy = 0;
const maxLoansBeforeBuy = 1;

function maxLoan(){
    let sum = balance - debt;
    if (sum < 0) return 0
    else return sum*2;
}

function requestLoan() {
    if (maxLoan() <= 0){
        console.log("Insufficient funds.");
        alert("Insufficient funds!");
        return false;
    }
    if (loansBeforeBuy >= maxLoansBeforeBuy){
        alert(`You've reached your maximum number of allowed loans! (${loansBeforeBuy}/${maxLoansBeforeBuy})
        \nReset by buying something, because that's totally how banks work.`);
        return false;
    }
    //Show popup prompt
    let requestedAmount = parseInt(prompt("You can loan up to "+maxLoan()+" kr. Enter desired amount.", 0));
    
    if (isNaN(requestedAmount)){
        console.log("Input must be a number.");
        return false;
    }
    if (requestedAmount > 2*balance){
        console.log("Denied. Request exceeds your maximum of "+maxLoan()+" kr.");
        return false;
    }

    balance += requestedAmount;
    debt += requestedAmount;
    console.log("Granted. Added ${balance}. Debt: ${debt}");
    loansBeforeBuy++;
    return true;
}

//#endregion

//#region WORK
let paycheck = 0;
function doWork(){
    const wage = 100;
    paycheck += wage; 
}
function doPaycheck(){
    balance += paycheck;
    paycheck = 0;
}
//#endregion

//#region LAPTOPS

function buySelected(){
    const pc = data[ddlLaptops.value];
    if (pc.price > balance){
        alert("Purchase failed!\nInsufficient balance.");
        return;
    }
    balance -= pc.price;
    loansBeforeBuy = 0;
    render();
    alert( `Purchase complete!\nYou've bought a ${pc.name}.
    \n\n${pc.price}kr has been deducted from account.`);
}

//#endregion



//#region INIT WEBSITE
const el = id => document.getElementById(id);
const labBalance = el("labBalance");
const labDebt = el("labDebt");
const labWork = el("labWork");

const pcFeatures = el("pcFeatures");
const pcName = el("pcName");
const pcPrice = el("pcPrice");
const pcDesc = el("pcDescription");
const pcImage = el("pcImage");

const btnLoan = el("btnLoan");
const btnWork = el("btnWork");
const btnDeposit = el("btnDeposit");
const btnBuy = el("btnBuy");

const ddlLaptops = el("laptops");

//Listeners
btnLoan.addEventListener("click", event => {
    requestLoan();
    render();
});

btnWork.addEventListener("click", event => {
    doWork();
    render();
});

btnDeposit.addEventListener("click", event => {
    doPaycheck();
    render();
});

btnBuy.addEventListener("click", event => {
    buySelected();
    render();
});

ddlLaptops.addEventListener("change", event => {
    render();
    console.log("Selected: "+ddlLaptops.value);
});

function readLaptopInventory(){
    /*
    const URL = "http://localhost:5500/data/laptops.json"
    const response = await fetch(URL)
    const laptops = response.json();
    */
    
    console.log(laptops);

    ddlLaptops.innerHTML = "";
    data.forEach((pc, i) => {
        const elItem = document.createElement("option");
        elItem.innerHTML = `${pc.name} - ${pc.price}kr`;
        elItem.value = i;
        ddlLaptops.appendChild(elItem);
    })   
}


//<select class="form-control" name="laptops" id="laptops">
//<option value="PC1">Nice Computer</option>

function render(){
    labBalance.innerHTML = balance+" kr";
    //labDebt.innerHTML = "Debt: "+debt+ " kr<br>";
    labWork.innerHTML = paycheck+" kr";

    const pc = data[ddlLaptops.value];
    pcFeatures.innerHTML = "";
    pc.features.forEach(f => {
        const elItem = document.createElement("li");
        elItem.innerHTML = f;
        pcFeatures.appendChild(elItem);
    })  

    pcName.innerText = pc.name;
    pcPrice.innerText = pc.price;
    pcDesc.innerText = pc.description;
    pcImage.src = "/data/"+pc.image;
}

function init(){
    
}


readLaptopInventory();
render();
